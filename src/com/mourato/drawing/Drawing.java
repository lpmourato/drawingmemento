package com.mourato.drawing;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.mourato.memento.CareTaker;
import com.mourato.memento.MementoController;
import com.mourato.memento.Originator;

public class Drawing extends JFrame {

	private Point mPointStart = null;
	private Point mPointEnd = null;
	CareTaker mCareTaker = new CareTaker();
	Originator mOriginator = new Originator();
	private JButton btnUndo;
	private JButton btnRedo;
	private ArrayList<short[]> points;
	
	public Drawing() {
		btnUndo = new JButton("Undo");
		btnUndo.setBounds(10, 10, 100, 40);
		btnUndo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				points = MementoController.getInstance().restoreMemento();
				repaint();
			}
		});
		this.add(btnUndo);
		
		btnRedo = new JButton("Redo");
		btnRedo.setBounds(150, 10, 100, 40);
		btnRedo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                points = MementoController.getInstance().redoAction();
                repaint();
            }
        });
		this.add(btnRedo);
		
		JPanel panel = new JPanel();
		panel.setSize(this.getWidth(), this.getHeight());
		panel.setBackground(Color.white);
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				mPointStart = e.getPoint();
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				mPointStart = null;
				mPointEnd = null;
				MementoController.getInstance().saveMemento();
			}
		});
		 panel.addMouseMotionListener(new MouseMotionAdapter() {
             public void mouseMoved(MouseEvent e) {
            	 //mPointEnd = e.getPoint();
             }

             public void mouseDragged(MouseEvent e) {
            	 mPointStart = mPointEnd;
            	 mPointEnd = e.getPoint();
                 repaint();
             }
         });
		 this.add(panel);
	}

	public void paint(Graphics g) {
		if (mPointStart != null) {
		    short x1 = (short)mPointStart.x;
		    short y1 = (short)mPointStart.y;
		    short x2 = (short)mPointEnd.x;
		    short y2 = (short)mPointEnd.y;
			MementoController.getInstance().setMemento(x1, y1, x2, y2);
			g.drawLine(x1, y1, x2, y2);
        } else if(points != null) {
        	g.setXORMode(getBackground());
        	for (short[] point : points) {
                g.drawLine(point[0], point[1], point[2], point[3]);
            }
        }
	}

	public static void main(String[] args) {
		Drawing dw = new Drawing();
		dw.setSize(600, 500);
		dw.setLocationRelativeTo(null);
		dw.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		dw.setResizable(false);
		dw.setVisible(true);
	}

}
