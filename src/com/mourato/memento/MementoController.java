package com.mourato.memento;

import java.util.ArrayList;

public class MementoController {

    private static MementoController instance;
    private Originator originator;
    private CareTaker careTaker;
    private int currentState;

    private MementoController() {
    	originator = new Originator();
    	careTaker = new CareTaker();
	}
    
    public static MementoController getInstance() {
        if(instance == null){
            instance = new MementoController();
        }
        return instance;
    }
    
    
    public void setMemento(short x1, short y1, short x2, short y2) {
        originator.set(x1, y1, x2, y2);
    }

    public void saveMemento() {
        careTaker.addMemento(originator.saveMemento());
        currentState++;
	}
    
    public ArrayList<short[]> restoreMemento() {
        ArrayList<short[]> list = new ArrayList<short[]>();
        if(currentState > 0){
            list = originator.restoreFromMemento(careTaker.getMemento(--currentState));
        }
        return list;
    }
    
    public ArrayList<short[]> redoAction() {
        ArrayList<short[]> list = new ArrayList<short[]>();
        if(currentState > -1 && currentState < careTaker.sizeState()){
            list = originator.restoreFromMemento(careTaker.getMemento(currentState++));
        }
        return list;
    }
}
