package com.mourato.memento;

import java.util.ArrayList;

public class CareTaker {

	private ArrayList<Memento> savedStates = new ArrayList<Memento>();
	
	public void addMemento(Memento memento) {
		savedStates.add(memento);
	}
	
	public Memento getMemento(int index) {
		return savedStates.get(index);
	}
	
	public int sizeState() {
        return savedStates.size();
    }
}
