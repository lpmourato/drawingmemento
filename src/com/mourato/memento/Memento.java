package com.mourato.memento;

import java.util.ArrayList;

public class Memento {

	private ArrayList<short[]> listPoints;
	
	public Memento(ArrayList<short[]> list) {
        listPoints = list;
    }

	public ArrayList<short[]> getSavedState() {
        return listPoints;
    }

	
}
