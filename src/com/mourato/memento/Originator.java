package com.mourato.memento;

import java.util.ArrayList;

public class Originator {

	private short[] points = new short[4];
	private ArrayList<short[]> listPoints = new ArrayList<>();
	public void set(short x1, short y1, short x2, short y2){
		points = new short[4];
		points[0] = x1;
		points[1] = y1;
		points[2] = x2;
		points[3] = y2;
		listPoints.add(points);
	}
	
	public Memento saveMemento() {
	    Memento memento = new Memento(listPoints);
	    listPoints = new ArrayList<short[]>();
	    return memento;
	}
	public ArrayList<short[]> restoreFromMemento(Memento memento) {
      listPoints = memento.getSavedState();
      return listPoints;
	}

}
